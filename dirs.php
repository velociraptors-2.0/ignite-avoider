<?php
/*

require_once ($_SERVER['DOCUMENT_ROOT'].'/dirs.php');
Requirer::require(fichero_a_requerir);

*/


define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT'] . '/');
define('VIEWS_PATH', ROOT_PATH . 'views/');
define('APP_PATH', ROOT_PATH . 'app/');
define('CTRL_PATH', APP_PATH . 'controllers/');
define('DTV_PATH', APP_PATH . 'directives/');
define('SRV_PATH', APP_PATH . 'services/');
define('CLASSES_PATH', ROOT_PATH . 'classes/');

//FILES
define('TEST_FILE', CLASSES_PATH . 'Test.php');
define('MAIN_JS', APP_PATH . 'js/main.js');