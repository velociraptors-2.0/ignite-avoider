angular.module('mainApp')
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl : "./views/v_main.html"
            })
            .when("/home", {
                redirectTo: '/'
            })
            .when("/actions", {
                templateUrl: './views/v_actions.html'
            })
            .when("/tips", {
                templateUrl: './views/v_tips.html'
            })
            .when("/safepath", {
                templateUrl: './views/v_safe_path.html'
            })
            .when("/sos", {
                templateUrl: './views/v_sos.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);