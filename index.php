<?php
//echo $_SERVER['REMOTE_ADDR'];
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
  
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.0.1/css/ol.css" type="text/css">
    <style>
      .map {
        height: 400px;
        width: 100%;
      }
    </style>
    <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.0.1/build/ol.js"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"> </script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>
    <link rel="stylesheet" href="css/main.css">
    
    <script src="app/app.js"></script>
    <script src="app/config.js"></script>
    <script src="app/controllers/mainController.js"></script>
    <script src="app/js/main.js"></script>
    <script src="https://kit.fontawesome.com/e42888c4e7.js" crossorigin="anonymous"></script>

    <script src="app/js/jquery-3.4.1.min.js"></script>


    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- <script src="app/directives/mainDtv.js"></script> -->
    <!-- <script src="app/services/mainSvc.js"></script> -->
    <!-- includes -->
    <script src="https://cdn.rawgit.com/geocodezip/geoxml3/master/polys/geoxml3.js"></script>

    <title>Ignite Avoid | Firefighting since 2019</title>
</head>

<body ng-app="mainApp">
<!--suppress HtmlUnknownAnchorTarget -->
<a href="#!home"><img src="rsc/img/ignite_avoider.PNG" class="img-fluid" alt="ignite_avoider" style="margin-bottom:0"/></a>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <button style="width: 100%" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <!--suppress HtmlUnknownAnchorTarget -->
                <a class="nav-link" href="#!home">Home</a>
            </li>
            <li class="nav-item">
                <!--suppress HtmlUnknownAnchorTarget -->
                <a class="nav-link" href="#!actions">Actions</a>
            </li>
        </ul>
    </div>
</nav>

    <div ng-view></div>

<div id="footer" class="jumbotron text-center" style="margin-bottom:0">
    <a href="tel:112" id="oneonetwo" class="btn rounded-circle btn-outline-primary">
        <i class="fa fa-phone" aria-hidden="true"></i>112
    </a>
    <p style="font-style: italic">Developed by <a style="color: #777;" href="#">Velociraptors</a></p>
</div>
</body>

</html>