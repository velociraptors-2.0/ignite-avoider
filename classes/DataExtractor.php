<?php
    class DataExtractor{
        // Paso 1 - Descargar fichero global.
        function downloadUrlToKMLFile()
        {

            $sTimeLastDownload = '../timeLastDownload.json';
            $nowtime = time();

            if (file_exists($sTimeLastDownload)) {
                echo "El fichero $sTimeLastDownload existe";

                $sContentTimeJSON = file_get_contents($sTimeLastDownload);
                $array = json_decode($sContentTimeJSON, true);
                $sTimeLastDownloadValue = $array["timeLastDownload"];

                //Se le suma una hora
                $sTimeLastDownloadValue = $sTimeLastDownloadValue + 60*60;

                if($sTimeLastDownloadValue <= $nowtime){
                    $bDownload = true;
                }else {
                    $bDownload = false;
                }


            } else {
                $fTimeLastDownload = fopen('../'.$sTimeLastDownload, 'w');
                fwrite($fTimeLastDownload, '{timeLastDownload : '. $nowtime .'}');
                $bDownload = true;
            }

            if($bDownload == true){
                $url = "https://firms.modaps.eosdis.nasa.gov/data/active_fire/viirs/kml/VNP14IMGTDL_NRT_Global_24h.kml";
                $outFileName = "../WWFires.kml";

                if(is_file($url)) {
                    copy($url, $outFileName);
                } else {
                    $options = array(
                        CURLOPT_FILE    => fopen($outFileName, 'w'),
                        CURLOPT_TIMEOUT =>  28800, // set this to 8 hours so we dont timeout on big files
                        CURLOPT_URL     => $url
                    );

                    $ch = curl_init();
                    curl_setopt_array($ch, $options);
                    curl_exec($ch);
                    curl_close($ch);
                }
            }
        }

        //Paso 2 - Obtener localización del dispositivo
        /*
            {
                Datos devueltos
                "ip": "1.1.1.1",
                "country_code2": "AU",
                "country_code3": "AUS",
                "country_name": "Australia",
                "state_prov": "Queensland",
                "district": "Brisbane",
                "city": "South Brisbane",
                "zipcode": "4101",
                "latitude": "-27.4748",
                "longitude": "153.017"
            }
        */



        /* getLocation($ip) {
            $apiKey = "7159643e12a249acbe3812e23ce60d6b";

            $url = "https://api.ipgeolocation.io/ipgeo?apiKey=".$apiKey."&ip=".$ip."&fields=geo";
            $cURL = curl_init();

            curl_setopt($cURL, CURLOPT_URL, $url);
            curl_setopt($cURL, CURLOPT_HTTPGET, true);
            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Accept: application/json'
            ));
            return json_decode(curl_exec($cURL), true);
        }*/

        //Paso 3 - Generar fichero KML de un radio de 15km.
        function distance($lat1, $lon1, $lat2, $lon2, $unit) {
            if (($lat1 == $lat2) && ($lon1 == $lon2)) {
                return 0;
            }
            else {
                $theta = $lon1 - $lon2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $unit = strtoupper($unit);

                if ($unit == "K") {
                    return ($miles * 1.609344);
                } else if ($unit == "N") {
                    return ($miles * 0.8684);
                } else {
                    return $miles;
                }
            }
        }

        function generate_KML15Km($ip) {

            $date_location = json_decode($this->getLocation($ip));

            $fn = fopen("../WWFires.kml","r");
            $contadorLines = 0;

            $latitude_geo = $date_location -> latitude;
            $longitude_geo = $date_location -> longitude;

            $contenido_KML15 = "";

            while(! feof($fn))  {
                $line = fgets($fn);

                if ($contadorLines > 12) {

                    $latitude_file = trim(substr($line, 99,13));
                    $longitude_file = trim(substr($line, 129,13));

                    $distance_KM = $this->distance($latitude_geo,$longitude_geo,$latitude_file,$longitude_file,"K");

                    if($distance_KM <= 15){
                        $contenido_KML15 = $contenido_KML15.$line."\n";
                    }

                }else {
                    $contenido_KML15 = $contenido_KML15.$line."\n";
                }

                $contadorLines = $contadorLines +1;
            }

            $contenido_KML15 = $contenido_KML15."</Folder></Document></kml>\n";

            $fp = fopen('../firesNear15Km.kml', 'w');
            fwrite($fp, $contenido_KML15);

            fclose($fp);
            fclose($fn);
        }



        //Paso 4 - DetailsFire - Request
        function call_weatherAPIConditions($latitude, $longitude){

            $url ='https://weather.cit.api.here.com/weather/1.0/report.json?product=observation&latitude='.$latitude.'&longitude='.$longitude.'&oneobservation=true&app_id=vkaOtbzJ0h61GfFYBoS5&app_code=RRN3nh0tFoRyeSRRrhIJvw';

            $ch = curl_init($url);
            // Configuring curl options
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array('Accept: application/json'),
                CURLOPT_SSL_VERIFYPEER => false,
            );
            // Setting curl options
            curl_setopt_array( $ch, $options );
            // Getting results
            $response = curl_exec($ch); // Getting jSON result string
            // Cerrar el recurso cURL y liberar recursos del sistema
            curl_close($ch);

            $data = json_decode($response, true);
            print_r($data);

            return $data;
        }
    }


	
?>