<?php
    Class Requirer{
        public static function get($classes){
            $classes_array = explode(',', $classes);
            foreach($classes_array as $class){
                /** @noinspection PhpIncludeInspection */
                require_once (CLASSES_PATH.$class.'.php');
            }
        }
    }
?>